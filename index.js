const express = require('express')
const path = require('path')
const app = express()
const port = process.env.PORT || 3000
const pessoas = require('./routes/pessoas')
const model = require('./models/index')
const bodyParser = require('body-parser')

//bodyparser
app.use(bodyParser.urlencoded({extended: true}))
//public
app.use(express.static('public'))

//view
app.set('views', path.join(__dirname, 'views'))
app.set('view engine', 'ejs')

//router
app.get('/', (req, res) => res.render('home'))
app.use('/pessoas', pessoas)


//listening
model.sequelize.sync().then(() =>{
    app.listen(port, () => console.log('CRUD-ORM listening'))
})

