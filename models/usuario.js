module.exports = (sequelize,  DataTypes) => {
    const Usuario = sequelize.define('Usuario', {
        username: DataTypes.STRING,
        senha: DataTypes.STRING,
        nascimento: DataTypes.DATE
    })
    return Usuario
}

